provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "airflow"{
  ami           = "ami-3f1d2729"
  instance_type = "t2.nano"
  key_name = "aws-res-scopussearchnp-keypair"
  vpc_security_group_ids = ["sg-626d9e12"]
  subnet_id = "subnet-8ffc9cd5"
  iam_instance_profile = "indexing_solr-abstract-np"
  count = 0
  user_data = "#!/bin/bash\ncd /tmp\nsudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm"
  tags {
    Name = "airflow-ssm-test"
  }
}




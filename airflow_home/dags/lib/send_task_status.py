import boto3
import logging

log = logging.getLogger(__name__)


def send_task_status(contex):
    task_params = get_parameters(contex)
    send_status = send_status_to_cloudwatch(task_params['DagID'],
                                            task_params['TaskID'],
                                            task_params['TaskStatus'])
    metric_response = send_status.put_metric_data_fun()
    alarm_response = send_status.put_metric_alarm_fun()
    return metric_response, alarm_response


def get_parameters(contex):
    dag_id = contex['task_instance'].dag_id
    task_id = contex['task_instance'].task_id
    task_status = contex['task_instance'].state
    return {'DagID': dag_id, 'TaskID': task_id, 'TaskStatus': task_status}


class send_status_to_cloudwatch(object):

    def __init__(self,
                 DagID,
                 TaskID,
                 TaskStatus,
                 MetricName='AirflowTaskStatuses',
                 MetricNamespace='Aiflow'):
        self.MetricName = MetricName
        self.MetricNamespace = MetricNamespace
        self.DagID = DagID
        self.TaskID = TaskID
        self.TaskStatus = TaskStatus
        self.AlarmName = ('AirflowStatus' + DagID + TaskID)
        self.AlarmDescription = ('AirflowStatus' + DagID + TaskID)
        self.client = boto3.client('cloudwatch', region_name='eu-west-1')

    def put_metric_data_fun(self):
        if self.TaskStatus == 'success':
            task_status_id = 0
        elif self.TaskStatus == 'failed':
            task_status_id = 1
        else:
            task_status_id = -1
        response = self.client.put_metric_data(
            Namespace=self.MetricNamespace,
            MetricData=[
                {
                    'MetricName': self.MetricName,
                    'Dimensions': [
                        {
                            'Name': 'DagID',
                            'Value': self.DagID,
                        },
                        {
                            'Name': 'TaskID',
                            'Value': self.TaskID
                        },
                    ],
                    'Value': task_status_id,
                    'Unit': 'Count'
                },
            ]
        )
        log.info("Put metric data:  DagID - %s, TaskID - %s, TaskStatus - %s, TaskID - %s\n"
                 % (self.DagID, self.TaskID, self.TaskStatus, self.TaskID))
        print('-' * 15)
        print(response)
        print('-' * 15)
        return response['ResponseMetadata']['HTTPStatusCode']

    def put_metric_alarm_fun(self):
        response = self.client.put_metric_alarm(
            AlarmName=self.AlarmName,
            AlarmDescription=self.AlarmDescription,
            ActionsEnabled=False,
            MetricName=self.MetricName,
            Namespace=self.MetricNamespace,
            Statistic='Minimum',
            Dimensions=[
                {
                    'Name': 'DagID',
                    'Value': self.DagID,
                },
                {
                    'Name': 'TaskID',
                    'Value': self.TaskID
                },
            ],
            Period=60,
            Unit='Count',
            EvaluationPeriods=1,
            Threshold=1,
            ComparisonOperator='GreaterThanOrEqualToThreshold',
            TreatMissingData='ignore'
        )
        print('-' * 15)
        print(response)
        print('-' * 15)
        return response['ResponseMetadata']['HTTPStatusCode']


if __name__ == '__main__':
    # create logger
    log = logging.getLogger('simple_test')
    log.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(levelname)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    log.addHandler(ch)

    DagID = 'Dag1'
    TaskID = 'Task2'
    TaskStatus = 'failed'
    log.debug('DagID: %s\n' % DagID)
    log.debug('TaskID: %s\n' % TaskID)
    log.debug('TaskStatus: %s\n' % TaskStatus)


    send_status_to_cloudwatch = send_status_to_cloudwatch(DagID,
                                                          TaskID,
                                                          TaskStatus)

    metric_response = send_status_to_cloudwatch.put_metric_data_fun()
    alarm_response = send_status_to_cloudwatch.put_metric_alarm_fun()

    print("Metric response: %s\n" % metric_response)
    print("Alarm response: %s\n" % alarm_response)

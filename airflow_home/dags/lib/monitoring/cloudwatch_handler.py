import boto3
import traceback
import logging

log = logging.getLogger(__name__)


class CloudwatchHandler(object):
    """
    A class to handle sending Airflow task state to Cloudwatch.

    It implements the 'send_task_state' method which does the following:
     - creates the alarm if one does not exist, or updates if one exists
     - sends either 0 for success, or 1 for failure

    """

    def __init__(self, metric_name, namespace, region_name):
        self.metric_name = metric_name
        self.namespace = namespace
        self.cloudwatch = boto3.client('cloudwatch', region_name=region_name)

    def _put_metric_alarm(self, alarm_name, dag_id, task_id):
        """
        A method creates a new alarm, or updates an existing alarm
        """
        try:
            alarm_description = 'Alarm for {}'.format(alarm_name)
            response = self.cloudwatch.put_metric_alarm(
                AlarmName=alarm_name,
                AlarmDescription=alarm_description,
                ActionsEnabled=True,
                MetricName=self.metric_name,
                Namespace=self.namespace,
                Statistic='Maximum',
                Dimensions=[
                    {
                        'Name': 'DagID',
                        'Value': dag_id,
                    },
                    {
                        'Name': 'TaskID',
                        'Value': task_id,
                    },
                ],
                Period=60,
                Unit='Count',
                EvaluationPeriods=1,
                Threshold=1,
                ComparisonOperator='GreaterThanOrEqualToThreshold',
                TreatMissingData='ignore'
            )
        except Exception as error:
            except_err = traceback.format_exc()
            response = {'ResponseMetadata': {'HTTPStatusCode': 0}}

        http_status_code = response['ResponseMetadata']['HTTPStatusCode']
        if http_status_code != 200 and http_status_code != 0:
            log.error('put_metric_alarm method failed with HTTPStatusCode: {}'.format(http_status_code))
        elif http_status_code == 0:
            log.error('put_metric_alarm method failed with the exception: \n {}'.format(except_err))
        elif http_status_code == 200:
            log.info('Metric Alarm was updated or created in cloudwatch. Alarm name: {4}, Metric name: {0}, Namespace: {1}, Dimensions {2}.{3}, dag_id: {2}, task_id: {3}'.format(self.metric_name, self.namespace, dag_id, task_id, alarm_name))

        return {'http_status_code': http_status_code}

    def _put_metric_data(self, dag_id, task_id, task_state):
        """
        A method sends metrics to CloudWatch
        - if task status success sends 0
        - if task status failed sends 1
        - if task is not success and not failed sends -1
        """
        if task_state == 'success':
            state_id = 0
        elif task_state == 'failed':
            state_id = 1
        else:
            state_id = -1

        try:
            response = self.cloudwatch.put_metric_data(
                Namespace=self.namespace,
                MetricData=[
                    {
                        'MetricName': self.metric_name,
                        'Dimensions': [
                            {
                                'Name': 'DagID',
                                'Value': dag_id,
                            },
                            {
                                'Name': 'TaskID',
                                'Value': task_id,
                            },
                        ],
                        'Value': state_id,
                        'Unit': 'Count'
                    },
                ]
            )
        except Exception as error:
            except_err = traceback.format_exc()
            response = {'ResponseMetadata': {'HTTPStatusCode': 0}}

        http_status_code = response['ResponseMetadata']['HTTPStatusCode']
        if http_status_code != 200 and http_status_code != 0:
            log.error('put_metric_data method failed with HTTPStatusCode: {}'.format(http_status_code))
        elif http_status_code == 0:
            log.error('put_metric_data method failed with the exception: \n {}'.format(except_err))
        elif http_status_code == 200:
            log.info('Metric Data was sent to cloudwatch. Metric name: {0}, Namespace: {1}, Dimensions {2}.{3}, dag_id: {2}, task_id: {3}, task_state: {4}, state_id: {5}'.format(self.metric_name, self.namespace, dag_id, task_id, task_state, state_id))

        return {'http_status_code': http_status_code, 'state_id': state_id}

    def send_task_state(self, context):
        """
        A method parses dag_id, task_id, task_state form context
        and runs _put_metric_data and put_metric_alarm methods
        """
        dag_id = context['task_instance'].dag_id
        task_id = context['task_instance'].task_id
        task_state = context['task_instance'].state
        alarm_name = '{0}-{1}-{2}'.format(self.metric_name, dag_id, task_id)

        for attempt in range(3):
            response = self._put_metric_alarm(alarm_name, dag_id, task_id)
            if response['http_status_code'] == 200:
                break

        for attempt in range(3):
            self._put_metric_data(dag_id, task_id, task_state)
            if response['http_status_code'] == 200:
                break

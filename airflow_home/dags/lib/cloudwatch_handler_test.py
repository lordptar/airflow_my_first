import logging
import cloudwatch_handler
from cloudwatch_handler import CloudwatchHandler

# create logger
logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
log.addHandler(ch)

dag_id = 'Dag1'
task_id = 'Task2'
task_status = 'failed'
metric_name = 'SC-1000'
namespace = 'Airflow'
region_name = 'eu-west-1'
alarm_name = (dag_id + task_id)
log.debug('DagID: {}'.format(dag_id))
log.debug('TaskID: {}'.format(task_id))
log.debug('TaskStatus: {}'.format(task_status))
log.debug('MetricName: {}'.format(metric_name))
log.debug('Namespace: {}'.format(namespace))
log.debug('Region name: {}'.format(region_name))

cloudwatch_handler = CloudwatchHandler(metric_name, namespace, region_name)

alarm_response = cloudwatch_handler._put_metric_alarm(alarm_name, dag_id, task_id)
metric_response = cloudwatch_handler._put_metric_data(dag_id, task_id, task_status)

print("Metric response: %s\n" % metric_response)
print("Alarm response: %s\n" % alarm_response)

import unittest
from moto import mock_cloudwatch
from send_task_status import send_status_to_cloudwatch


class SendStatusTestCase(unittest.TestCase):
    @mock_cloudwatch
    def test_send_status(self):
        instance_cloudwatch = send_status_to_cloudwatch('Dag1',
                                                        'Task',
                                                        'failed')
        metric_response = instance_cloudwatch.put_metric_data_fun()
        alarm_response = instance_cloudwatch.put_metric_alarm_fun()
        print(metric_response)
        assert metric_response == 200
        assert alarm_response == 200


if __name__ == '__main__':
    unittest.main()

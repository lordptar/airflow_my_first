import boto3
import logging
import time
import traceback

log = logging.getLogger(__name__)


def http_status_handler(method_name, response, except_err=None, success_message=None):
    http_status_code = response['ResponseMetadata']['HTTPStatusCode']
    if http_status_code != 200 and http_status_code != 0:
        log.error('The {0} method failed with HTTPStatusCode: {1}'.format(method_name, http_status_code))
    elif http_status_code == 0:
        log.error('The {0} method failed with the exception: \n {1}'.format(method_name, except_err))
    elif http_status_code == 200 and success_message:
        log.info(success_message)


class SSMCommandsHandler(object):
    """
    A class to handle SSM commands simultaneously
    """

    def __init__(self, region_name, max_errors='10%', execution_timeout='172800'):
        self.region_name = region_name
        self.ssm_client = boto3.client('ssm', self.region_name)
        self.max_errors = max_errors
        self.execution_timeout = execution_timeout

    def _send_ssm_command(self, ssm_cmd, ec2_tag_name):
        """
        A method to run SSM commands simultaneously
        """
        except_err = None
        try:
            response = self.ssm_client.send_command(
                Targets=[
                    {
                        'Key': 'tag:Name',
                        'Values': [
                            ec2_tag_name
                        ]
                    },
                ],
                DocumentName='AWS-RunShellScript',
                Comment='IndexUpdate_{}'.format(ec2_tag_name),
                Parameters={
                    'executionTimeout': [self.execution_timeout],
                    'commands': [
                        ssm_cmd,
                    ]
                },
                MaxErrors=self.max_errors,
            )
        except Exception as error:
            except_err = traceback.format_exc()
            response = {'ResponseMetadata': {'HTTPStatusCode': 0}}

        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            success_message = 'SSM commnads have started with CommandId: {}'.format(response['Command']['CommandId'])
        else:
            success_message = None
        http_status_handler('SSM send_command', response, except_err, success_message)

        return response

    def _list_ssm_commands(self, ssm_cmd_id):
        except_err = None
        try:
            response = self.ssm_client.list_commands(
                CommandId=ssm_cmd_id,
            )
        except Exception as error:
            except_err = traceback.format_exc()
            response = {'ResponseMetadata': {'HTTPStatusCode': 0}}

        http_status_handler('SSM list_commands', response, except_err)

        return response

    def _list_command_invocations(self, ssm_cmd_id):
        except_err = None
        try:
            response = self.ssm_client.list_command_invocations(
                CommandId=ssm_cmd_id,
            )
        except Exception as error:
            except_err = traceback.format_exc()
            response = {'ResponseMetadata': {'HTTPStatusCode': 0}}

        http_status_handler('SSM list_commands', response, except_err)

        return response

    def _get_failed_instances(self, invocations_response):
        list_ids_and_ips = 'list of failed instances: '
        for instance in invocations_response['CommandInvocations']:
            if instance['Status'] in ['Failed', 'TimedOut', 'Cancelled']:
                instance_id = instance['InstanceId']
                instance_ip = instance['InstanceName'][3:].replace("-", ".")
                list_ids_and_ips +='{0}({1}),'.format(instance_id, instance_ip)
        log.info(list_ids_and_ips)

    def _get_run_details(self, ssm_list_response, log_level):
        ssm_cmd_status = ssm_list_response['Commands'][0]['Status']
        targets_count = ssm_list_response['Commands'][0]['TargetCount']
        completed_count = ssm_list_response['Commands'][0]['CompletedCount']
        error_count = ssm_list_response['Commands'][0]['ErrorCount']
        custom_log = getattr(log, log_level)
        custom_log('status: {}'.format(ssm_cmd_status))
        custom_log('targets_count: {}'.format(targets_count))
        custom_log('completed_count: {}'.format(completed_count))
        custom_log('error_count: {}'.format(error_count))

    def _check_status_cmd(self, ssm_cmd_id, ssm_cmd_status, wait):
        while ssm_cmd_status not in ['Success', 'Cancelled', 'Failed', 'TimedOut']:
            for attempt in range(3):
                ssm_list_response = self._list_ssm_commands(ssm_cmd_id)
                http_status_code_list = ssm_list_response['ResponseMetadata']['HTTPStatusCode']
                if http_status_code_list == 200:
                        break

            if http_status_code_list == 200:
                ssm_cmd_status = ssm_list_response['Commands'][0]['Status']
                self._get_run_details(ssm_list_response, 'debug')
                time.sleep(wait)

    def __call__(self, ssm_cmd, ec2_tag_name, wait=5):
        for attempt in range(3):
            ssm_cmd_response = self._send_ssm_command(ssm_cmd, ec2_tag_name)
            http_status_code_cmd = ssm_cmd_response['ResponseMetadata']['HTTPStatusCode']
            if http_status_code_cmd == 200:
                break
        if http_status_code_cmd == 200:
            ssm_cmd_id = ssm_cmd_response['Command']['CommandId']
            ssm_cmd_status = ssm_cmd_response['Command']['Status']

            log.debug('ssm_cmd_id: {}'.format(ssm_cmd_id))

            self._check_status_cmd(ssm_cmd_id, ssm_cmd_status, wait)

            for attempt in range(3):
                ssm_list_response = self._list_ssm_commands(ssm_cmd_id)
                http_status_code_list = ssm_list_response['ResponseMetadata']['HTTPStatusCode']
                if http_status_code_list == 200:
                    break

            if http_status_code_list == 200:
                ssm_cmd_status = ssm_list_response['Commands'][0]['Status']
                self._get_run_details(ssm_list_response, 'info')

            if ssm_cmd_status in ['Failed', 'TimedOut', 'Cancelled']:
                for attempt in range(3):
                    invocations_response = self._list_command_invocations(ssm_cmd_id)
                    http_status_code_invocations = invocations_response['ResponseMetadata']['HTTPStatusCode']
                    if http_status_code_invocations == 200:
                        break
                if http_status_code_invocations == 200:
                    self._get_failed_instances(invocations_response)
                raise ValueError('SSM command exited with an error. SSM command status: {0}, error_count: {1}'.format(ssm_cmd_status, ssm_list_response['Commands'][0]['ErrorCount']))


if __name__ == '__main__':
    def main():
        ssm_cmd_success = 'echo "le lion"; exit 0'
        ssm_cmd_timedout = 'echo "le lion"; sleep 3600; exit 0'
        ssm_cmd_failed = 'echo "le lion"; exit 1'
        ssm_cmd_random = 'echo "le lion"; exit $(( RANDOM % 2 ))'
        region_name = 'us-east-1'
        instance_tag = 'airflow-ssm-test'

        log.debug('-' * 15)
        log.debug('Full Susses')
        ssm_commands_handler = SSMCommandsHandler(region_name)
        try:
            ssm_commands_handler(ssm_cmd_success, instance_tag, wait=2)
        except Exception as error:
            print(traceback.format_exc())

        log.debug('-' * 15)
        log.debug('TimedOut')
        ssm_commands_handler = SSMCommandsHandler(region_name, execution_timeout='5')
        try:
            ssm_commands_handler(ssm_cmd_timedout, instance_tag)
        except Exception as error:
            print(traceback.format_exc())

        log.debug('-' * 15)
        log.debug('Full fail')
        ssm_commands_handler = SSMCommandsHandler(region_name)
        try:
            ssm_commands_handler(ssm_cmd_failed, instance_tag)
        except Exception as error:
            print(traceback.format_exc())

        log.debug('-' * 15)
        log.debug('Random fail')
        try:
            ssm_commands_handler(ssm_cmd_random, instance_tag)
        except Exception as error:
            print(traceback.format_exc())
    # create logger
    log.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(levelname)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    log.addHandler(ch)

    main()

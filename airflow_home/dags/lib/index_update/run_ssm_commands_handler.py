import logging
import traceback
import sys
from ssm_commands_handler import SSMCommandsHandler

# create logger
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logging.getLogger('lib.monitoring.cloudwatch_handler').setLevel(logging.DEBUG)
logging.getLogger('boto3').setLevel(logging.CRITICAL)
logging.getLogger('botocore').setLevel(logging.CRITICAL)

log = logging.getLogger()


if __name__ == '__main__':
    def main():
        ssm_cmd_success = 'echo "le lion"; exit 0'
        ssm_cmd_timedout = 'echo "le lion"; sleep 3600; exit 0'
        ssm_cmd_failed = 'echo "le lion"; exit 1'
        ssm_cmd_random = 'echo "le lion"; exit $(( RANDOM % 2 ))'
        region_name = 'us-east-1'
        instance_tag = 'airflow-ssm-test'

        log.debug('-' * 15)
        log.debug('Full Susses')
        ssm_commands_handler = SSMCommandsHandler(region_name)
        try:
            ssm_commands_handler(ssm_cmd_success, instance_tag, wait=2)
        except Exception as error:
            print(traceback.format_exc())

        log.debug('-' * 15)
        log.debug('TimedOut')
        ssm_commands_handler = SSMCommandsHandler(region_name, execution_timeout='5')
        try:
            ssm_commands_handler(ssm_cmd_timedout, instance_tag)
        except Exception as error:
            print(traceback.format_exc())

        log.debug('-' * 15)
        log.debug('Full fail')
        ssm_commands_handler = SSMCommandsHandler(region_name)
        try:
            ssm_commands_handler(ssm_cmd_failed, instance_tag)
        except Exception as error:
            print(traceback.format_exc())

        log.debug('-' * 15)
        log.debug('Random fail')
        try:
            ssm_commands_handler(ssm_cmd_random, instance_tag)
        except Exception as error:
            print(traceback.format_exc())

    main()

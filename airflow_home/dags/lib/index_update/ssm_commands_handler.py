import boto3
import logging
import time
import traceback
from botocore.exceptions import ClientError


log = logging.getLogger(__name__)


class SSMCmdException(Exception):
    """
    SSM command Exception
    """
    pass


class SSMCommandsHandler(object):
    """
    A class to handle SSM commands simultaneously
    """

    def __init__(self, region_name, max_errors='10%', execution_timeout='172800'):
        self.region_name = region_name
        self.ssm_client = boto3.client('ssm', self.region_name)
        self.max_errors = max_errors
        self.execution_timeout = execution_timeout

    def _send_ssm_command(self, ssm_cmd, ec2_tag_name):
        """
        A method runs the boto3 command send_command.
        Which runs SSM commands simultaneously by using EC2 tags
        """
        response = self.ssm_client.send_command(
            Targets=[
                {
                    'Key': 'tag:Name',
                    'Values': [
                        ec2_tag_name
                    ]
                },
            ],
            DocumentName='AWS-RunShellScript',
            Comment='{0} servers are targeted'.format(ec2_tag_name),
            Parameters={
                'executionTimeout': [self.execution_timeout],
                'commands': [
                    ssm_cmd,
                ]
            },
            MaxErrors=self.max_errors,
        )
        log.info('SSM command({0}) ran agianst {1} servers'.format(
            ssm_cmd, ec2_tag_name))

        return response

    def _list_ssm_commands(self, ssm_cmd_id):
        """
        A method runs boto3 command list_commands
        Which get info about SSM command:
        - Status SSM command
        - TargetCount
        - CompletedCount
        - ErrorCount
        """
        response = self.ssm_client.list_commands(
            CommandId=ssm_cmd_id,
        )
        return response

    def _send_failed_instances_to_log(self, ssm_cmd_id):
        """
        A method sends failed instance's ids and ips to the log
        """
        ids_ips_dict = {}
        try:
            response = self.ssm_client.list_command_invocations(
                CommandId=ssm_cmd_id,
            )
        except ClientError as e:
            log.error(traceback.format_exc())
            response = None
            return False

        if response:
            list_ids_and_ips = 'list of failed instances: '
            for instance in response['CommandInvocations']:
                if instance['Status'] in ['Failed', 'TimedOut', 'Cancelled']:
                    instance_id = instance['InstanceId']
                    instance_ip = instance['InstanceName'][3:].replace("-", ".")
                    list_ids_and_ips += '{0}({1}),'.format(instance_id, instance_ip)
                    ids_ips_dict[instance_id] = instance_ip
            log.info(list_ids_and_ips)
        return ids_ips_dict

    def _get_run_details(self, ssm_list_response, log_level):
        """
        A method gets info from the boto3 command list_commands:
        - Status SSM command
        - TargetCount
        - CompletedCount
        - ErrorCount
        """
        ssm_cmd_status = ssm_list_response['Commands'][0]['Status']
        targets_count = ssm_list_response['Commands'][0]['TargetCount']
        completed_count = ssm_list_response['Commands'][0]['CompletedCount']
        error_count = ssm_list_response['Commands'][0]['ErrorCount']
        custom_log = getattr(log, log_level)
        custom_log('status: {}'.format(ssm_cmd_status))
        custom_log('targets_count: {}'.format(targets_count))
        custom_log('completed_count: {}'.format(completed_count))
        custom_log('error_count: {}'.format(error_count))
        return True

    def _check_status_cmd(self, ssm_cmd_id, ssm_cmd_status, wait):
        """
        A method checks status of SSM command in loop
        """
        while ssm_cmd_status not in ['Success', 'Cancelled', 'Failed', 'TimedOut']:
            ssm_list_response = self._list_ssm_commands(ssm_cmd_id)
            ssm_cmd_status = ssm_list_response['Commands'][0]['Status']
            self._get_run_details(ssm_list_response, 'debug')
            time.sleep(wait)

    def __call__(self, ssm_cmd, ec2_tag_name, wait=5):
        """
        A method runs all helpers together
        - if SSM command Success the method will return True
        - if SSM command fails or timeout or Cancelled
           the method will raise the exception SSMCmdException
        """
        ssm_cmd_response = self._send_ssm_command(ssm_cmd, ec2_tag_name)
        ssm_cmd_id = ssm_cmd_response['Command']['CommandId']
        ssm_cmd_status = ssm_cmd_response['Command']['Status']
        log.info('SSM commnads have started with CommandId: {}'.format(ssm_cmd_id))
        log.debug('ssm_cmd_id: {}'.format(ssm_cmd_id))

        self._check_status_cmd(ssm_cmd_id, ssm_cmd_status, wait)

        ssm_list_response = self._list_ssm_commands(ssm_cmd_id)
        ssm_cmd_status = ssm_list_response['Commands'][0]['Status']

        self._get_run_details(ssm_list_response, 'info')

        if ssm_cmd_status in ['Failed', 'TimedOut', 'Cancelled']:
            self._send_failed_instances_to_log(ssm_cmd_id)
            raise SSMCmdException(''.join([
                'SSM command exited with an error. ',
                'SSM command status: {0}, error_count: {1}'.format(
                    ssm_cmd_status,
                    ssm_list_response['Commands'][0]['ErrorCount'])
                ])
            )
        return True

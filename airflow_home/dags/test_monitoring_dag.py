import airflow
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
from airflow.models import DAG
from lib.send_task_status import send_task_status
from lib.send_task_status import get_parameters
from lib.send_task_status import send_status_to_cloudwatch
import pickle
import simplejson as json
from lib.monitoring.cloudwatch_handler import CloudwatchHandler

CloudwatchHandler = CloudwatchHandler('Airflow', 'SC-1000', 'eu-west-1')


def send_task_status_test(contex):

    task_params = get_parameters(contex)
    send_status = send_status_to_cloudwatch(task_params['DagID'],
                                            task_params['TaskID'],
                                            task_params['TaskStatus'])
    print('DagID: %s\n' % task_params['DagID'])
    print('DagID: %s\n' % task_params['TaskID'])
    print('TaskStatus: %s\n' % task_params['TaskStatus'])
    metric_response = send_status.put_metric_data_fun()
    alarm_response = send_status.put_metric_alarm_fun()
    print('metric_response: %s\n' % metric_response)
    print('alarm_response: %s\n' % alarm_response)


def print_hi():
    print_it = "print ---hello---"
    print(print_it)


def open_file():
    with open('/tmp/lets_read', 'r') as file2:
        output = file2.read()
        print(output)


args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(2)
}


dag = DAG('mon_dag', default_args=args, schedule_interval=None)

task1 = PythonOperator(
    task_id='print_hi',
    python_callable=print_hi,
    on_success_callback=CloudwatchHandler.send_task_state,
    on_failure_callback=send_task_status_test,
    dag=dag,)


task2 = PythonOperator(
    task_id='open_file',
    python_callable=open_file,
    on_success_callback=send_task_status_test,
    on_failure_callback=send_task_status_test,
    dag=dag,)

t_main = BashOperator(
    task_id='java1',
    dag=dag,
    # bash_command='java -jar /tmp/hello.jar',
    bash_command='ls -la',
    on_failure_callback=send_task_status_test,
    on_success_callback=send_task_status_test
)

#!/usr/bin/env python
from builtins import range

import airflow
from airflow.operators.python_operator import PythonOperator
from airflow.models import DAG
import time
import re
import logging
from lib.index_update.ssm_commands_handler import SSMCommandsHandler

log = logging.getLogger(__name__)

def print_p(task_id, **context):
    print_it = "printing p"
    print("-" * 10 + print_it)
    value = context['task_instance'].xcom_pull(task_ids='pav_task2')
    print("-" * 10 + str(value))
    print("-" * 10 + str(re.split('\W+', str(context['task']))[3]))
    return ("log print " + print_it + task_id)


def print_r():
    print_it = "printing r"
    print(print_it)
    return print_it


def print_task_name():
    print('Task is ' + task)


def my_sleeping_function(random_base):
    """This is a function that will run within the DAG execution"""
    print("-" * 30 + str(random_base))
    time.sleep(random_base)
    return random_base


def fail_if_no_file():
    with open('/tmp/created', 'w') as file1:
        file1.write('success 1')
    try:
        with open('/tmp/sem', 'r') as file2:
                output = file2.read()
                print(output)
    except IOError as e:
        print(e.errno)
        print(e)


args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(2)
}


def args_test_fun(arg1, arg2, arg3):
    print('arg1: {}'.format(arg1))
    print('arg2: {}'.format(arg2))
    print('arg3: {}'.format(arg3))

def ssm_test():
    ssm_cmd_success = 'echo "le lion"; exit 0'
    ssm_cmd_failed = 'echo "le lion"; exit 1'
    ssm_cmd_random = 'echo "le lion"; exit $(( RANDOM % 2 ))'
    region_name = 'us-east-1'
    instance_tag = 'airflow-ssm-test'

    log.debug('-' * 15)
    log.debug('Full Susses')
    ssm_commands_handler = SSMCommandsHandler(ssm_cmd_success, region_name)
    ssm_commands_handler(instance_tag)

    log.debug('-' * 15)
    log.debug('Full fail')
    ssm_commands_handler = SSMCommandsHandler(ssm_cmd_failed, region_name)
    ssm_commands_handler(instance_tag)

    log.debug('-' * 15)
    log.debug('Random fail')
    ssm_commands_handler = SSMCommandsHandler(ssm_cmd_random, region_name)
    ssm_commands_handler(instance_tag)


dag = DAG('pav_dag', default_args=args, schedule_interval=None)

task1 = PythonOperator(
    task_id='pav_task1',
    python_callable=print_p,
    provide_context=True,
    op_kwargs={'task_id': "pav_task"},
    dag=dag,)


task2 = PythonOperator(
    task_id='pav_task2',
    python_callable=ssm_test,
    dag=dag,)

fail_task = PythonOperator(
    task_id='fail_task',
    python_callable=fail_if_no_file,
    dag=dag,)

args_test = PythonOperator(
    task_id='args_test',
    python_callable=args_test_fun,
    op_kwargs={'arg1': 'var1', 'arg2': 'var2', 'arg3': 'var3'},
    dag=dag,)

# print_task_name = PythonOperator(
#     task_id='print_task_name',
#     python_callable=fail_if_no_file,
#     dag=dag,)


# for i in range(3):
#     task = PythonOperator(
#         task_id='sleep_for_' + str(i),
#         python_callable=my_sleeping_function,
#         op_kwargs={'random_base': float(i) / 3},
#         dag=dag)

args_test >> task1 >> task2

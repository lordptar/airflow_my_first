from __future__ import print_function
from builtins import range
import airflow
from airflow.operators.python_operator import PythonOperator
from airflow.models import DAG
import urllib

args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(2)
}

http_path = "https://bitbucket.org/lordptar/airflow_my_first/raw/f1c713224030f4b12f315e98561be9b025ae028f/java_dir/hello.jar"


def download():
    urllib.urlretrieve(http_path, "/tmp/hello.jar")


dag = DAG(
    dag_id='download', default_args=args,
    schedule_interval=None)

download = PythonOperator(
    task_id='download',
    python_callable=download,
    dag=dag)


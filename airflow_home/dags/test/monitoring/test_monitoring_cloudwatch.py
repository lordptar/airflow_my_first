import logging
import os
import sys
import unittest

sys.path.insert(0, '../../')
from lib.monitoring.cloudwatch_handler import CloudwatchHandler
from mockups import mock_make_api_call
from moto import mock_cloudwatch
from mock import patch

logging.basicConfig(stream=sys.stdout, level=logging.CRITICAL)
logging.getLogger('lib.monitoring.cloudwatch_handler').setLevel(logging.CRITICAL)
logging.getLogger('boto3').setLevel(logging.CRITICAL)
logging.getLogger('botocore').setLevel(logging.CRITICAL)

os.environ["AWS_ACCESS_KEY_ID"] = "key_id"
os.environ["AWS_SECRET_ACCESS_KEY"] = "secret_key"


class SendStatusTestCase(unittest.TestCase):
    """
    A unittest of CloudwatchHandler
    """
    api_path = 'lib.monitoring.{}.boto3.session.botocore.client.BaseClient._make_api_call'
    dag_id = 'Dag1'
    task_id = 'Task1'
    metric_name = 'Airflow'
    namespace = 'SC-1000'
    region_name = 'eu-west-1'
    alarm_name = 'Superb'
    instance_cloudwatch = CloudwatchHandler(metric_name, namespace, region_name)

    @mock_cloudwatch
    def test_put_metric_data(self):
        """
        A method for testing the _put_metric_data method
         - The test covers success, failed and unknown task statuses
         - The test covers 200, 403 and exceptions for the put_metric_data boto method
        """
        metric_response = self.instance_cloudwatch._put_metric_data(self.dag_id,
                                                                    self.task_id,
                                                                    'failed')
        self.assertEqual(metric_response['http_status_code'], 200)
        self.assertEqual(metric_response['state_id'], 1)

        metric_response = self.instance_cloudwatch._put_metric_data(self.dag_id,
                                                                    self.task_id,
                                                                    'success')
        self.assertEqual(metric_response['http_status_code'], 200)
        self.assertEqual(metric_response['state_id'], 0)

        metric_response = self.instance_cloudwatch._put_metric_data(self.dag_id,
                                                                    self.task_id,
                                                                    'unknown')
        self.assertEqual(metric_response['http_status_code'], 200)
        self.assertEqual(metric_response['state_id'], -1)

        with patch(self.api_path.format('cloudwatch_handler'), new=mock_make_api_call) as mock_boto3:

            fail_response = self.instance_cloudwatch._put_metric_data('unauthorized',
                                                                      self.task_id,
                                                                      'success')
            self.assertEqual(fail_response['http_status_code'], 403)

            metric_response = self.instance_cloudwatch._put_metric_data('exceptions',
                                                                        self.task_id,
                                                                        'unknown')
            self.assertEqual(metric_response['http_status_code'], 0)

    @mock_cloudwatch
    def test_put_metric_alarm(self):
        """
        A method for testing the _put_metric_alarm
         - The test covers 200, 403 and exceptions for the put_metric_alarm boto method
        """

        alarm_response = self.instance_cloudwatch._put_metric_alarm(self.alarm_name,
                                                                    self.dag_id,
                                                                    self.task_id)

        self.assertEqual(alarm_response['http_status_code'], 200)

        with patch(self.api_path.format('cloudwatch_handler'), new=mock_make_api_call) as mock_boto3:
            fail_response = self.instance_cloudwatch._put_metric_alarm('unauthorized',
                                                                       self.dag_id,
                                                                       self.task_id)
            self.assertEqual(fail_response['http_status_code'], 403)

            fail_response = self.instance_cloudwatch._put_metric_alarm('exceptions',
                                                                       self.dag_id,
                                                                       self.task_id)
            self.assertEqual(fail_response['http_status_code'], 0)

    @mock_cloudwatch
    def test_send_task_state(self):
        """
        A method for testing the send_task_state method
        """
        class task_instance(object):
            def __init__(self):
                self.dag_id = 'Dag1'
                self.task_id = 'Task1'
                self.state = 'failed'

        task_instance = task_instance()
        context = {'task_instance': task_instance}

        self.instance_cloudwatch.send_task_state(context)


if __name__ == '__main__':
    unittest.main()

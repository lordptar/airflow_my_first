from botocore.exceptions import ClientError

def mock_make_api_call(self, operation_name, kwargs):
    """
    Helper function to mock responses from botocore client
    """
    if operation_name == 'PutMetricAlarm':
        alarm_name = kwargs['AlarmName']
        if alarm_name == 'unauthorized':
            return_value = {
                'ResponseMetadata': {
                    'HTTPStatusCode': 403,
                }
            }
        elif alarm_name == 'exceptions':
            return_value = {'Error': {'Code': 'InvalidClientTokenId', 'Message': "An exception's error occurred"}}
            raise ClientError(return_value, operation_name)
    elif operation_name == 'PutMetricData':
        dag_id = kwargs['MetricData'][0]['Dimensions'][0]['Value']
        if dag_id == 'unauthorized':
            return_value = {
                'ResponseMetadata': {
                    'HTTPStatusCode': 403,
                }
            }
        elif dag_id == 'exceptions':
            return_value = {'Error': {'Code': 'InvalidClientTokenId', 'Message': "An exception's error occurred"}}
            raise ClientError(return_value, operation_name)
    else:
        return_value = None

    return return_value

import logging
import sys
import unittest

sys.path.insert(0, '../../')
from botocore.exceptions import ClientError
from lib.index_update.ssm_commands_handler import SSMCommandsHandler
from lib.index_update.ssm_commands_handler import SSMCmdException
from mockups import mock_make_api_call
from mock import patch

logging.basicConfig(stream=sys.stdout, level=logging.CRITICAL)
logging.getLogger('lib.monitoring.cloudwatch_handler').setLevel(logging.CRITICAL)
logging.getLogger('boto3').setLevel(logging.CRITICAL)
logging.getLogger('botocore').setLevel(logging.CRITICAL)

api_path = ''.join(['lib.index_update.{}.boto3.',
                    'session.botocore.client.',
                    'BaseClient._make_api_call'
                    ])


class SendStatusTestCase(unittest.TestCase):
    """
    A unittest of SSMCommandsHandler
    """

    @patch(api_path.format('ssm_commands_handler'), new=mock_make_api_call)
    def test_send_ssm_command(self):
        """
        A method for testing the _send_ssm_command method
        - The test cover success which is HTTPStatusCode = 200
        - The test cover the ClientError exception
        """
        ssm_commands_handler = SSMCommandsHandler('us-east-1')
        response = ssm_commands_handler._send_ssm_command('Success', 'ec2-tag')
        self.assertEqual(response['ResponseMetadata']['HTTPStatusCode'], 200)
        with self.assertRaises(ClientError):
            ssm_commands_handler._send_ssm_command('Exceptions', 'ec2-tag')

    @patch(api_path.format('ssm_commands_handler'), new=mock_make_api_call)
    def test_list_ssm_commands(self):
        """
        A method for testing the _list_ssm_commands method
        - The test cover success which is HTTPStatusCode = 200
        - The test cover the ClientError exception
        """
        ssm_commands_handler = SSMCommandsHandler('us-east-1')
        response = ssm_commands_handler._list_ssm_commands('Success')
        self.assertEqual(response['ResponseMetadata']['HTTPStatusCode'], 200)
        with self.assertRaises(ClientError):
            ssm_commands_handler._list_ssm_commands('Exceptions')

    @patch(api_path.format('ssm_commands_handler'), new=mock_make_api_call)
    def test_send_failed_instances_to_log(self):
        """
        A method for testing the _send_failed_instances_to_log method
        - The test cover the ClientError exception
        """
        ssm_commands_handler = SSMCommandsHandler('us-east-1')
        response = ssm_commands_handler._send_failed_instances_to_log('Failed')
        expected_dict = {'i-id-1': '10.20.30.40',
                         'i-id-2': '20.20.30.40',
                         'i-id-3': '30.20.30.40'}

        self.assertDictEqual(expected_dict, response)
        response = ssm_commands_handler._send_failed_instances_to_log('Exceptions')
        self.assertFalse(response)


    @patch(api_path.format('ssm_commands_handler'), new=mock_make_api_call)
    def test__call__(self):
        """
        A method for testing the _send_failed_instances_to_log method
        - The test covers success running SSM command
        - The test covers the SSMCmdException exception
        """
        ssm_commands_handler = SSMCommandsHandler('us-east-1')
        response = ssm_commands_handler('Success', 'ec2-tag', wait=1)
        self.assertTrue(response)
        with self.assertRaises(SSMCmdException):
            ssm_commands_handler('Failed', 'ec2-tag', wait=1)


if __name__ == '__main__':
    unittest.main()

from botocore.exceptions import ClientError


def mock_make_api_call(self, operation_name, kwargs):
    """
    Helper function to mock responses from botocore client
    """
    if operation_name == 'SendCommand':
        ssm_cmd = kwargs['Parameters']['commands'][0]
        if ssm_cmd == 'Success':
            return_value = {
                'Command': {
                    'CommandId': 'Success',
                    'Status': 'Pending'
                    },
                'ResponseMetadata': {
                    'HTTPStatusCode': 200,
                }
            }
        elif ssm_cmd == 'Failed':
            return_value = {
                'Command': {
                    'CommandId': 'Failed',
                    'Status': 'Pending'
                    },
                'ResponseMetadata': {
                    'HTTPStatusCode': 200,
                }
            }
        elif ssm_cmd == 'Exceptions':
            return_value = {'Error':
                            {'Code': 'InvalidClientTokenId',
                             'Message': "An exception's error occurred"}}
            raise ClientError(return_value, operation_name)
        else:
            return_value = None

    elif operation_name == 'ListCommands':
        ssm_cmd_id = kwargs['CommandId']
        if ssm_cmd_id == 'Success':
            return_value = {
                'Commands': [{'Status': 'Success',
                              'TargetCount': 5,
                              'CompletedCount': 5,
                              'ErrorCount': 0
                              }],
                'ResponseMetadata': {
                    'HTTPStatusCode': 200,
                }
            }
        elif ssm_cmd_id in ['Failed', 'TimedOut', 'Cancelled']:
            return_value = {
                'Commands': [{'Status': 'Failed',
                              'TargetCount': 5,
                              'CompletedCount': 5,
                              'ErrorCount': 3
                              }],
                'ResponseMetadata': {
                    'HTTPStatusCode': 200,
                }
            }
        elif ssm_cmd_id == 'Exceptions':
            return_value = {'Error':
                            {'Code': 'InvalidClientTokenId',
                             'Message': "An exception's error occurred"}}
            raise ClientError(return_value, operation_name)
        else:
            return_value = None

    elif operation_name == 'ListCommandInvocations':
        ssm_cmd_id = kwargs['CommandId']
        if ssm_cmd_id in ['Failed', 'TimedOut', 'Cancelled']:
            return_value = {
                'CommandInvocations': [
                    {'Status': 'Failed',
                     'InstanceId': 'i-id-1',
                     'InstanceName': 'ip-10-20-30-40'
                     },
                    {'Status': 'Failed',
                     'InstanceId': 'i-id-2',
                     'InstanceName': 'ip-20-20-30-40'
                     },
                    {'Status': 'Failed',
                     'InstanceId': 'i-id-3',
                     'InstanceName': 'ip-30-20-30-40'
                     }
                ],
                'ResponseMetadata': {
                    'HTTPStatusCode': 200,
                }
            }
        elif ssm_cmd_id == 'Exceptions':
            return_value = {'Error':
                            {'Code': 'InvalidClientTokenId',
                             'Message': "An exception's error occurred"}}
            raise ClientError(return_value, operation_name)
        else:
            return_value = None

    else:
        return_value = None

    return return_value

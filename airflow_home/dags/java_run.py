import airflow
from airflow.operators.bash_operator import BashOperator
from airflow.models import DAG


args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(2)
}


dag = DAG('java_run', default_args=args, schedule_interval=None)


def send_task_status(contex):
    dag_id = contex['task_instance'].dag_id
    task_id = contex['task_instance'].task_id
    task_status = contex['task_instance'].state
    print("DAG: %s" % dag_id)
    print("Task ID: %s" % task_id)
    print("Task status: %s" % task_status)


t_main = BashOperator(
    task_id='java1',
    dag=dag,
#   bash_command='java -jar /tmp/hello.jar',
    bash_command='ls -la',
    on_failure_callback=send_task_status,
    on_success_callback=send_task_status
)
